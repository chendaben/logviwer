#!/bin/bash

dev="device"

devices=$(adb devices)

if [[ ${devices} == *$dev ]]
then
   echo "手机已经连接好终端"
   info=$(adb shell dumpsys activity top | awk -F " " '/TASK/ {print $2}')
   echo "当前运行app的包名是:${info}"
   python loger.py ${info} --min-level 'E'
else
  echo "手机没有连接好终端"
fi
